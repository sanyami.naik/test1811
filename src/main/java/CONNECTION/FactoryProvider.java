package CONNECTION;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class FactoryProvider {
    public static Connection connect() {

        Connection connection;
        try {
            connection = DriverManager.getConnection("jdbc:mysql://localhost:3306/test1811","root","Root@12");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return connection;
    }
}
