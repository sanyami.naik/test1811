package CONNECTION;

import ENTITY.Student;
import DAO.daoImplementation;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.util.List;

public class main {
        static BufferedReader bufferedReader=new BufferedReader(new InputStreamReader(System.in));


        public static void main(String[] args) throws IOException, SQLException {
//            Configuration configuration=new Configuration().configure();
//            SessionFactory sessionFactory=configuration.buildSessionFactory();
//            Session session=sessionFactory.openSession();


            while(true) {

                daoImplementation dao = new daoImplementation();
                System.out.println("1-For adding new student");
                System.out.println("2-For findingAll");
                System.out.println("3-For finding by id");
                System.out.println("4-For deleting by id");
                System.out.println("5-For deleting all");
                System.out.println("6- For exiting");
                System.out.println("Enter the option you want");

                int option = Integer.parseInt(bufferedReader.readLine());

                switch (option) {
                    case 1:

                        System.out.println("Enter the student id");
                        int id=Integer.parseInt(bufferedReader.readLine());
                        System.out.println("Enter the student name");
                        String name=bufferedReader.readLine();
                        Student student=new Student(id,name);
                        dao.save(student);
                        break;

                    case 2:
                        List<Student> students = dao.findAll();
                        System.out.println(students.size());
                        System.out.println("The list of the students is as follows");
                        students.stream().forEach(System.out::println);
                        break;

                    case 3:
                        System.out.println("Enter the id of the student you want to find information of");
                        int id1=Integer.parseInt(bufferedReader.readLine());
                        Student student1 = dao.findById(id1);
                        System.out.println("The student is "+student1);
                        break;

                    case 4:
                        System.out.println("Enter the id of the student you want to delete");
                        int id2=Integer.parseInt(bufferedReader.readLine());
                        dao.deleteById(id2);
                        System.out.println("The student has been deleted");
                        break;

                    case 5:
                        dao.deleteAll();
                        System.out.println("The information has been deleted");
                        break;

                    case 6:
                        System.exit(0);


                }


            }
    }
}
