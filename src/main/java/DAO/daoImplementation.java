package DAO;

import ENTITY.Student;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import CONNECTION.FactoryProvider;


public class daoImplementation implements dao{

    public void save(Student student) throws SQLException {
        Connection connection = FactoryProvider.connect();
        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery("Select * from student order by studentId");
        int count=0;

        while(resultSet.next())
        {
            if(student.getStudentId()==resultSet.getInt(1))
            {
                count=1;
                break;
            }

        }
        if(count==0) {

            PreparedStatement preparedStatement = connection.prepareStatement("Insert into student values (?,?)");
            preparedStatement.setInt(1, student.getStudentId());
            preparedStatement.setString(2, student.getStudentName());
            preparedStatement.execute();
            System.out.println("The student inserted successfully");
        }
        else {
            PreparedStatement preparedStatement = connection.prepareStatement("Update Student set studentName=? where studentId="+student.getStudentId());
            preparedStatement.setString(1, student.getStudentName());
            preparedStatement.execute();
            System.out.println("The student updated successfully");
        }
    }


    public Student findById(int id) throws SQLException {
        Connection connection = FactoryProvider.connect();
        ResultSet resultSet ;
        PreparedStatement preparedStatement = connection.prepareStatement("Select * from Student " + "where studentId = ?");
        preparedStatement.setInt(1,id);

        resultSet = preparedStatement.executeQuery();
        Student student = new Student();

        while (resultSet.next()) {
            student.setStudentId(resultSet.getInt(1));
            student.setStudentName(resultSet.getString(2));
        }
        return student;
    }

    public List<Student> findAll() throws SQLException {
        Connection connection = FactoryProvider.connect();
        Statement statement=connection.createStatement();
        ResultSet resultSet=statement.executeQuery("Select * from student order by studentId");
        List<Student> studentList = new ArrayList();

        while (resultSet.next()) {
            Student student = new Student();
            student.setStudentId(resultSet.getInt(1));
            student.setStudentName(resultSet.getString(2));
            studentList.add(student);
        }
        return studentList;
    }

    public void deleteById(int id) throws SQLException {
        Connection connection = FactoryProvider.connect();
        Statement statement=connection.createStatement();
        statement.execute("Delete from student where studentId="+id);
    }



    public void deleteAll() throws SQLException {
        Connection connection = FactoryProvider.connect();
        Statement statement=connection.createStatement();
        statement.execute("Delete from student");
    }



}



/*OUTPUT:

1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
2
2
The list of the students is as follows
Student{studentId=1, studentName='Sanyaminaikkkkkk'}
Student{studentId=2, studentName='Ayushman'}
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
1
Enter the student id
2
Enter the student name
Ayushman Shuklaaa
The student updated successfully
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
2
2
The list of the students is as follows
Student{studentId=1, studentName='Sanyaminaikkkkkk'}
Student{studentId=2, studentName='Ayushman Shuklaaa'}
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
3
Enter the id of the student you want to find information of
2
The student is Student{studentId=2, studentName='Ayushman Shuklaaa'}
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
1
Enter the student id
3
Enter the student name
Praveer
The student inserted successfully
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
1
Enter the student id
4
Enter the student name
Ashraff
The student inserted successfully
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
1
Enter the student id
5
Enter the student name
Aman
The student inserted successfully
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
1
Enter the student id
5
Enter the student name
Aman Agarwal
The student updated successfully
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
2
5
The list of the students is as follows
Student{studentId=1, studentName='Sanyaminaikkkkkk'}
Student{studentId=2, studentName='Ayushman Shuklaaa'}
Student{studentId=3, studentName='Praveer'}
Student{studentId=4, studentName='Ashraff'}
Student{studentId=5, studentName='Aman Agarwal'}
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
4
Enter the id of the student you want to delete
3
The student has been deleted
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
2
4
The list of the students is as follows
Student{studentId=1, studentName='Sanyaminaikkkkkk'}
Student{studentId=2, studentName='Ayushman Shuklaaa'}
Student{studentId=4, studentName='Ashraff'}
Student{studentId=5, studentName='Aman Agarwal'}
1-For adding new student
2-For findingAll
3-For finding by id
4-For deleting by id
5-For deleting all
6- For exiting
Enter the option you want
6

Process finished with exit code 0

 */