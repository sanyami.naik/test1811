package DAO;

import ENTITY.Student;

import java.sql.SQLException;
import java.util.List;


public interface dao {
    List<Student> findAll() throws SQLException;
    public Student findById(int id) throws SQLException;


}
